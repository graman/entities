<?xml version="1.0" encoding="UTF-8"?>
<language namespace="EntityInstance" uuid="6d16a23c-68a7-4be5-ae4f-7d917e3a55b0">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot path="${module}/languageModels" />
    </modelRoot>
  </models>
  <accessoryModels />
  <generators>
    <generator generatorUID="EntityInstance#4711519129169083792" uuid="4d2ffb87-6508-4906-9888-22aefa0a462c">
      <models>
        <modelRoot contentPath="${module}" type="default">
          <sourceRoot path="${module}/generator/template" />
        </modelRoot>
      </models>
      <external-templates />
      <usedLanguages>
        <usedLanguage>6d16a23c-68a7-4be5-ae4f-7d917e3a55b0(EntityInstance)</usedLanguage>
        <usedLanguage>b401a680-8325-4110-8fd3-84331ff25bef(jetbrains.mps.lang.generator)</usedLanguage>
        <usedLanguage>d7706f63-9be2-479c-a3da-ae92af1e64d5(jetbrains.mps.lang.generator.generationContext)</usedLanguage>
      </usedLanguages>
      <usedDevKits>
        <usedDevKit>fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)</usedDevKit>
      </usedDevKits>
      <mapping-priorities />
    </generator>
  </generators>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">9548c6c7-de5a-47f1-8d8a-c6a61b743c08(EntityLanguage)</dependency>
  </dependencies>
  <usedLanguages>
    <usedLanguage>9548c6c7-de5a-47f1-8d8a-c6a61b743c08(EntityLanguage)</usedLanguage>
  </usedLanguages>
  <usedDevKits>
    <usedDevKit>2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)</usedDevKit>
  </usedDevKits>
  <extendedLanguages>
    <extendedLanguage>9548c6c7-de5a-47f1-8d8a-c6a61b743c08(EntityLanguage)</extendedLanguage>
  </extendedLanguages>
</language>

