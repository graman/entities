<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:88323e03-9750-4311-88f4-37f0efa571b7(EntityLanguage.structure)" version="6">
  <persistence version="8" />
  <language namespace="c72da2b9-7cce-4447-8389-f407dc1158b7(jetbrains.mps.lang.structure)" />
  <devkit namespace="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  <import index="tpce" modelUID="r:00000000-0000-4000-0000-011c89590292(jetbrains.mps.lang.structure.structure)" version="0" implicit="yes" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="n8lx" modelUID="r:88323e03-9750-4311-88f4-37f0efa571b7(EntityLanguage.structure)" version="6" implicit="yes" />
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233277854867" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="Entity" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="entity" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="7180291233278181508" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="attribute" />
      <property name="sourceCardinality" nameId="tpce.1071599893252" value="0..n" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="7180291233278164056" resolveInfo="EntityAttribute" />
    </node>
    <node role="implements" roleId="tpce.1169129564478" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="4711519129169084936" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="4711519129169084859" resolveInfo="IReferenceContent" />
    </node>
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233277870892" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="EntityResource" />
    <property name="rootable" nameId="tpce.1096454100552" value="true" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="Represents a package of resources" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="7180291233277870988" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="entities" />
      <property name="sourceCardinality" nameId="tpce.1071599893252" value="0..n" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="4711519129169084859" resolveInfo="IReferenceContent" />
    </node>
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278164056" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="EntityAttribute" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="Represents an attribute" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="7180291233278181490" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="aggregation" />
      <property name="role" nameId="tpce.1071599776563" value="type" />
      <property name="sourceCardinality" nameId="tpce.1071599893252" value="1" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="7180291233278164299" resolveInfo="Type" />
    </node>
    <node role="implements" roleId="tpce.1169129564478" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="7180291233278164110" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="tpck.1169194658468" resolveInfo="INamedConcept" />
    </node>
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278164299" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="Type" />
    <property name="abstract" nameId="tpce.4628067390765956802" value="true" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="tpck.1133920641626" resolveInfo="BaseConcept" />
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278346481" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="PrimitiveType" />
    <property name="abstract" nameId="tpce.4628067390765956802" value="true" />
    <property name="final" nameId="tpce.4628067390765956807" value="false" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="7180291233278164299" resolveInfo="Type" />
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278350017" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="StringType" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="string" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="A String type" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="7180291233278346481" resolveInfo="PrimitiveType" />
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278369221" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="IntType" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="int" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="An integer value" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="7180291233278346481" resolveInfo="PrimitiveType" />
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278854773" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="BoolType" />
    <property name="conceptAlias" nameId="tpce.5092175715804935370" value="bool" />
    <property name="conceptShortDescription" nameId="tpce.4628067390765907488" value="boolean" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="7180291233278346481" resolveInfo="PrimitiveType" />
  </root>
  <root type="tpce.ConceptDeclaration" typeId="tpce.1071489090640" id="7180291233278854997" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="EntityType" />
    <link role="extends" roleId="tpce.1071489389519" targetNodeId="7180291233278164299" resolveInfo="Type" />
    <node role="linkDeclaration" roleId="tpce.1071489727083" type="tpce.LinkDeclaration" typeId="tpce.1071489288298" id="7180291233278855114" nodeInfo="ig">
      <property name="metaClass" nameId="tpce.1071599937831" value="reference" />
      <property name="role" nameId="tpce.1071599776563" value="entity" />
      <property name="sourceCardinality" nameId="tpce.1071599893252" value="1" />
      <link role="target" roleId="tpce.1071599976176" targetNodeId="7180291233277854867" resolveInfo="Entity" />
    </node>
  </root>
  <root type="tpce.InterfaceConceptDeclaration" typeId="tpce.1169125989551" id="4711519129169084859" nodeInfo="ig">
    <property name="name" nameId="tpck.1169194664001" value="IReferenceContent" />
    <node role="extends" roleId="tpce.1169127546356" type="tpce.InterfaceConceptReference" typeId="tpce.1169127622168" id="4711519129169084913" nodeInfo="ig">
      <link role="intfc" roleId="tpce.1169127628841" targetNodeId="tpck.1169194658468" resolveInfo="INamedConcept" />
    </node>
  </root>
</model>

