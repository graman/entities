package EntityLanguage.typesystem;

/*Generated by MPS */

import jetbrains.mps.lang.typesystem.runtime.AbstractNonTypesystemRule_Runtime;
import jetbrains.mps.lang.typesystem.runtime.NonTypesystemRule_Runtime;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.typesystem.inference.TypeCheckingContext;
import jetbrains.mps.lang.typesystem.runtime.IsApplicableStatus;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.errors.messageTargets.MessageTarget;
import jetbrains.mps.errors.messageTargets.NodeMessageTarget;
import jetbrains.mps.errors.IErrorReporter;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.internal.collections.runtime.IWhereFilter;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.smodel.SModelUtil_new;

public class checkNamesRule_NonTypesystemRule extends AbstractNonTypesystemRule_Runtime implements NonTypesystemRule_Runtime {
  public checkNamesRule_NonTypesystemRule() {
  }

  public void applyRule(final SNode entityAttribute, final TypeCheckingContext typeCheckingContext, IsApplicableStatus status) {
    if (!(SNodeOperations.isInstanceOf(SNodeOperations.getParent(entityAttribute), "EntityLanguage.structure.Entity"))) {
      {
        MessageTarget errorTarget = new NodeMessageTarget();
        IErrorReporter _reporter_2309309498 = typeCheckingContext.reportWarning(entityAttribute, "Cannot compare non-entity please upgrade", "r:f99d8c5a-4d40-48d3-a0d9-2f2c6ac3527f(EntityLanguage.typesystem)", "4711519129173173796", null, errorTarget);
      }
      return;
    }
    if (ListSequence.fromList(SNodeOperations.getChildren(SNodeOperations.cast(SNodeOperations.getParent(entityAttribute), "EntityLanguage.structure.Entity"))).where(new IWhereFilter<SNode>() {
      public boolean accept(SNode it) {
        return SPropertyOperations.getString(SNodeOperations.cast(it, "EntityLanguage.structure.EntityAttribute"), "name") == SPropertyOperations.getString(entityAttribute, "name");
      }
    }).count() > 1) {
      {
        MessageTarget errorTarget = new NodeMessageTarget();
        IErrorReporter _reporter_2309309498 = typeCheckingContext.reportTypeError(entityAttribute, "duplicate name", "r:f99d8c5a-4d40-48d3-a0d9-2f2c6ac3527f(EntityLanguage.typesystem)", "7180291233280150616", null, errorTarget);
      }
    }
  }

  public String getApplicableConceptFQName() {
    return "EntityLanguage.structure.EntityAttribute";
  }

  public IsApplicableStatus isApplicableAndPattern(SNode argument) {
    {
      boolean b = SModelUtil_new.isAssignableConcept(argument.getConcept().getQualifiedName(), this.getApplicableConceptFQName());
      return new IsApplicableStatus(b, null);
    }
  }

  public boolean overrides() {
    return false;
  }
}
