package EntityLanguage.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.nodeEditor.cells.EditorCell_Constant;

public class IntType_Editor extends DefaultNodeEditor {
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createCollection_niawz5_a(editorContext, node);
  }

  public EditorCell createInspectedCell(EditorContext editorContext, SNode node) {
    return this.createCollection_niawz5_a_0(editorContext, node);
  }

  private EditorCell createCollection_niawz5_a(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createIndent2(editorContext, node);
    editorCell.setCellId("Collection_niawz5_a");
    editorCell.setBig(true);
    editorCell.addEditorCell(this.createComponent_niawz5_a0(editorContext, node));
    return editorCell;
  }

  private EditorCell createComponent_niawz5_a0(EditorContext editorContext, SNode node) {
    EditorCell editorCell = editorContext.getCellFactory().createEditorComponentCell(node, "jetbrains.mps.lang.core.editor.alias");
    return editorCell;
  }

  private EditorCell createCollection_niawz5_a_0(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createVertical(editorContext, node);
    editorCell.setCellId("Collection_niawz5_a_0");
    editorCell.setBig(true);
    editorCell.addEditorCell(this.createConstant_niawz5_a0(editorContext, node));
    return editorCell;
  }

  private EditorCell createConstant_niawz5_a0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "integer");
    editorCell.setCellId("Constant_niawz5_a0");
    editorCell.setDefaultText("");
    return editorCell;
  }
}
