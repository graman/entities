<?xml version="1.0" encoding="UTF-8"?>
<solution name="TestSolution" uuid="745513d7-ea9b-4eb6-911c-e5435cf966c2" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <usedLanguages>
    <usedLanguage>6d16a23c-68a7-4be5-ae4f-7d917e3a55b0(EntityInstance)</usedLanguage>
    <usedLanguage>9548c6c7-de5a-47f1-8d8a-c6a61b743c08(EntityLanguage)</usedLanguage>
    <usedLanguage>f61473f9-130f-42f6-b98d-6c438812c2f6(jetbrains.mps.baseLanguage.unitTest)</usedLanguage>
  </usedLanguages>
</solution>

