<?xml version="1.0" encoding="UTF-8"?>
<model modelUID="r:af1b601f-7d88-4788-acf4-cba1742af58e(TestSolution.One@tests)">
  <persistence version="8" />
  <language namespace="f61473f9-130f-42f6-b98d-6c438812c2f6(jetbrains.mps.baseLanguage.unitTest)" />
  <language namespace="9548c6c7-de5a-47f1-8d8a-c6a61b743c08(EntityLanguage)" />
  <language namespace="6d16a23c-68a7-4be5-ae4f-7d917e3a55b0(EntityInstance)" />
  <import index="tpck" modelUID="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" version="0" implicit="yes" />
  <import index="n8lx" modelUID="r:88323e03-9750-4311-88f4-37f0efa571b7(EntityLanguage.structure)" version="6" implicit="yes" />
  <import index="qjrr" modelUID="r:e66ada19-299f-4745-a984-48f62d04a23d(EntityInstance.structure)" version="1" implicit="yes" />
  <root type="n8lx.EntityResource" typeId="n8lx.7180291233277870892" id="5608543942732316477" nodeInfo="ng">
    <node role="entities" roleId="n8lx.7180291233277870988" type="n8lx.Entity" typeId="n8lx.7180291233277854867" id="5608543942732316529" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="Test" />
      <node role="attribute" roleId="n8lx.7180291233278181508" type="n8lx.EntityAttribute" typeId="n8lx.7180291233278164056" id="5608543942732316531" nodeInfo="ng">
        <property name="name" nameId="tpck.1169194664001" value="name" />
        <node role="type" roleId="n8lx.7180291233278181490" type="n8lx.StringType" typeId="n8lx.7180291233278350017" id="5608543942732316535" nodeInfo="ng" />
      </node>
      <node role="attribute" roleId="n8lx.7180291233278181508" type="n8lx.EntityAttribute" typeId="n8lx.7180291233278164056" id="5608543942732316538" nodeInfo="ng">
        <property name="name" nameId="tpck.1169194664001" value="name2" />
        <node role="type" roleId="n8lx.7180291233278181490" type="n8lx.StringType" typeId="n8lx.7180291233278350017" id="5608543942732316540" nodeInfo="ng" />
      </node>
      <node role="attribute" roleId="n8lx.7180291233278181508" type="n8lx.EntityAttribute" typeId="n8lx.7180291233278164056" id="5608543942732316545" nodeInfo="ng">
        <property name="name" nameId="tpck.1169194664001" value="test" />
        <node role="type" roleId="n8lx.7180291233278181490" type="n8lx.EntityType" typeId="n8lx.7180291233278854997" id="5608543942732316554" nodeInfo="ng">
          <link role="entity" roleId="n8lx.7180291233278855114" targetNodeId="5608543942732316529" resolveInfo="Test" />
        </node>
      </node>
      <node role="attribute" roleId="n8lx.7180291233278181508" type="n8lx.EntityAttribute" typeId="n8lx.7180291233278164056" id="5608543942732880021" nodeInfo="ng">
        <property name="name" nameId="tpck.1169194664001" value="kilo" />
        <node role="type" roleId="n8lx.7180291233278181490" type="n8lx.StringType" typeId="n8lx.7180291233278350017" id="5608543942732880023" nodeInfo="ng" />
      </node>
    </node>
    <node role="entities" roleId="n8lx.7180291233277870988" type="qjrr.EntityReference" typeId="qjrr.4711519129169235853" id="5608543942732316625" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="orange" />
      <link role="entity" roleId="qjrr.4711519129169246635" targetNodeId="5608543942732316529" resolveInfo="Test" />
      <node role="values" roleId="qjrr.4711519129169273534" type="qjrr.EntityAttributeValue" typeId="qjrr.4711519129169246640" id="5608543942732316642" nodeInfo="ng">
        <link role="values" roleId="qjrr.4711519129169271350" targetNodeId="5608543942732316538" resolveInfo="name2" />
        <node role="expr" roleId="qjrr.4711519129169311835" type="qjrr.StringConstExpression" typeId="qjrr.4711519129169273628" id="5608543942732316646" nodeInfo="ng" />
      </node>
    </node>
    <node role="entities" roleId="n8lx.7180291233277870988" type="qjrr.EntityReference" typeId="qjrr.4711519129169235853" id="5608543942732316573" nodeInfo="ng">
      <property name="name" nameId="tpck.1169194664001" value="apple" />
      <link role="entity" roleId="qjrr.4711519129169246635" targetNodeId="5608543942732316529" resolveInfo="Test" />
      <node role="values" roleId="qjrr.4711519129169273534" type="qjrr.EntityAttributeValue" typeId="qjrr.4711519129169246640" id="5608543942732316583" nodeInfo="ng">
        <link role="values" roleId="qjrr.4711519129169271350" targetNodeId="5608543942732316531" resolveInfo="name" />
        <node role="expr" roleId="qjrr.4711519129169311835" type="qjrr.StringConstExpression" typeId="qjrr.4711519129169273628" id="5608543942732316587" nodeInfo="ng" />
      </node>
      <node role="values" roleId="qjrr.4711519129169273534" type="qjrr.EntityAttributeValue" typeId="qjrr.4711519129169246640" id="5608543942732316590" nodeInfo="ng">
        <link role="values" roleId="qjrr.4711519129169271350" targetNodeId="5608543942732316538" resolveInfo="name2" />
        <node role="expr" roleId="qjrr.4711519129169311835" type="qjrr.StringConstExpression" typeId="qjrr.4711519129169273628" id="5608543942732316596" nodeInfo="ng" />
      </node>
      <node role="values" roleId="qjrr.4711519129169273534" type="qjrr.EntityAttributeValue" typeId="qjrr.4711519129169246640" id="5608543942732316599" nodeInfo="ng">
        <link role="values" roleId="qjrr.4711519129169271350" targetNodeId="5608543942732316545" resolveInfo="test" />
        <node role="expr" roleId="qjrr.4711519129169311835" type="qjrr.InstanceRefExpression" typeId="qjrr.4711519129178050274" id="5608543942732316667" nodeInfo="ng">
          <link role="instance" roleId="qjrr.4711519129183583492" targetNodeId="5608543942732316625" resolveInfo="orange" />
        </node>
      </node>
    </node>
  </root>
</model>

